# -*- coding: utf-8; -*-

# Set up extlookup if used:
$extlookup_datadir = '@@modulename@@/tests/testdata'
$extlookup_precedence = [ 'password' ]


# This comment marks the beginning of example usage.

# Use the manifest we're testing itself here:
include @@classref@@

info("important_variable is: ${@@classref@@::important_variable}")

# Include required modules here:
include ::dependency

# This comment marks the end of example usage.


# Declare required resources here:

