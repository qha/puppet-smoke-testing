Smoke testing setup instructions:

*   Merge the contents of the modules directory with your modules
    directory.

*   Create a test manifest using as much as possible of your manifest
    while still going through test application without killing puppet.
   
*   Include required modules after the modules or resources that need
    them, that way you'll know that your manifests work regardless of
    what order your external node classifier spits out classes in.
   
*   If you need several runs of puppet apply to get sufficient coverage
    or if you need to pass environment variables to your test
    manifests you can put sets of arguments for env, one set per line
    plus a unique word at the beginning of each such line, in
    _yourmodule/tests/yourclass.testenvs_. Each variable name will be
    prefixed with `FACTER_`. Lines with `#` as first non whitespace
    character and lines containing only whitespace are ignored.

    Example:

        # Production:
        pallasno  node=pallas ena=no
        pallasyes node=pallas ena=yes
        athenano  node=athena ena=no
        athenayes node=athena ena=yes
        
        # Test:
        pizzayes node=pizza ena=yes
        pistayes node=pista ena=yes

*   If you come across undefined variables that are needed in most
    manifests you can define them one per line in
    _smoke-testing-state/share/global.testenvdefs_. These will also be
    prefixed with `FACTER_` before being passed via env to the test
    run. Lines with `#` as first non whitespace character and lines
    containing only whitespace are ignored.
   
*   Run smoke tests for just your test manifest by calling:

        make yourmodule/tests/yourmanifest.testresult
   
*   Add dummy definitions to your test manifest to silence as many
    warnings and errors as feasible and repeat.
   
*   Copy the remaining warnings and errors to
    _yourmodule/tests/yourmanifest.filter_ escaping all characters
    with special meaning in regexen.
   
*   If you come across a warning that will feature in almost all smoke
    tests you can put it in the global filter in
    _smoke-testing-state/share/global.filter_ used with all test
    manifests.
   
*   Do smoke testing for the entire repo with:

        make -skj2

    Where 2 is a number less than or equal to the number of cores on
    your machine (use `facter processorcount`).
   
*   If smoke testing the entire repo reports errors you can re run the
    tests for just the modules you've repaired with:

        make -skj2 module/ ...

    (Do put the trailing `/` after each module or make will do
    nothing.)


A test manifest template is available as a make target, just call
`make yourmodule/tests/yourclass.pp` and a test manifest with broken
dependencies will be produced for you to repair.
